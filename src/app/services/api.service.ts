import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { Respuesta } from '../interfaces/respuesta-interface';



@Injectable({
  providedIn: 'root'
})
export class ApiService {

  rutaBase = 'http://fer-sepulveda.cl/api/api-prueba2.php';

  modeloUsuario: string;
  modeloContrasena: string;
  activo: string;

  constructor(private http: HttpClient, private router: Router) { }

  validarLogin(correo, contrasena) {
    return  this.http.get<Respuesta>(this.rutaBase + '?nombreFuncion=UsuarioLogin&usuario=' + correo + "&contrasena=" + contrasena);
  }

  crearUsuario(correo, contrasena) {
    return  this.http.post(this.rutaBase, { nombreFuncion: 'UsuarioAlmacenar', parametros: [correo, contrasena] });
  } 

  modificarContrasena(correo, contrasena) {
    return this.http.put(this.rutaBase, { nombreFuncion: "UsuarioModificarContrasena", parametros: { usuario: correo, contrasena: contrasena} });
  }
 

  
}