import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, NavigationExtras, Router } from '@angular/router';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  modeloUsuario: string;
  modeloContrasena: string;
  activo: string;

  constructor(private router: Router,  private sqlite: SQLite) {
    this.sqlite.create({
      name: 'datos.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS USUARIO(MAIL VARCHAR(75), CONTRASENA VARCHAR(30))', []).then(() => {
        console.log('tabla creada ok')
      }).catch(e => {
        console.log('tabla fallo');
      })
    }).catch(e => {
      console.log('base de datos fallo');
    })
   }

  

    canActivate(route: ActivatedRouteSnapshot): boolean { 



      var navegacionActual = this.router.getCurrentNavigation();
        
        this.modeloUsuario = navegacionActual.extras.state.nombre;
        this.modeloContrasena = navegacionActual.extras.state.pass;

        
          if (this.activo === 'si') {

            
            
            return true;

          } else {
            this.router.navigate(['login']);
            return false;
          }  
        
      

      
    }

  

  registrarUsuario(correo, contrasena){
    this.sqlite.create({
      name: 'datos.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO USUARIO VALUES(?, ?)', [correo, contrasena]).then(() => {
        console.log('Usuario Guardado')
      }).catch(e => {
        console.log('Usuario No Guardado');
      })
    }).catch(e => {
      console.log('base de datos fallo');
    })
  }

  validarRegistro(correo){
    return this.sqlite.create({
      name: 'datos.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      return db.executeSql('SELECT COUNT(MAIL) AS CANTIDAD FROM USUARIO WHERE MAIL = ?', [correo]).then((data) => {
        
        if(data.rows.item(0).CANTIDAD === 0){
          return false; //correo no existe
        }

        return true;
      }).catch(e => {
        return true;
      })
    }).catch(e => {
      return true;
    });
  }

  validarLogin(correo, contrasena){
    return this.sqlite.create({
      name: 'datos.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      return db.executeSql('SELECT COUNT(MAIL) AS CANTIDADMAIL FROM USUARIO WHERE MAIL = ? AND CONTRASENA = ?', [correo, contrasena]).then((data) => {
        
        if(data.rows.item(0).CANTIDADMAIL === 1){
          this.activo = 'si';
          return false; //validacion correcta
        }
        return true;
      }).catch(e => {
        return true;
      })
    }).catch(e => {
      return true;
    });
  }


  cambiarContrasena(nuevaContrasena, contrasena){
    return this.sqlite.create({
      name: 'datos.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      return db.executeSql('UPDATE USUARIO SET CONTRASENA = ? WHERE CONTRASENA = ?', [nuevaContrasena, contrasena]).then((data) => {
      }).catch(e => {
        return true;
      })
    }).catch(e => {
      return true;
    });
  }


  cerrarSesion(){
    this.activo = 'no';
  }

  
}
