import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { DbService } from 'src/app/services/db.service';
import { Camera, CameraResultType } from '@capacitor/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  nombreUsuario: string;
  modeloUsuario: string;
  modeloContrasena: string;
  ruta: string = '';
  texto: string = '';

  constructor(public router: Router, public alertController: AlertController, public navController: NavController,
    public api: ApiService, private dbService: DbService, private qr: BarcodeScanner) {
    this.nombreUsuario = this.router.getCurrentNavigation().extras.state.nombre
   
   }

  ngOnInit() {
  }

  async tomarFoto(){
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    });

    this.ruta = image.webPath;
  }


  botonQr(){
    this.qr.scan().then(data => {
      this.texto = data['text'];
      this.presentAlertaQr();
    })
    

  }

  async presentAlertaQr() {
    const alert = await this.alertController.create({
      header: 'Correcto!',
      message: this.texto,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            console.log(this.texto);
          }
        }
      ]
    });

    await alert.present();
  }

  cambiarContrasena(){

    var navigationExtras: NavigationExtras = {
      state: {
        nombre: this.modeloUsuario,
        pass: this.modeloContrasena
      }
    }
    this.router.navigate(['recuperar'], navigationExtras);
  }


  cerrarSesion(){
    this.dbService.cerrarSesion();
    this.router.navigate(['login']);
  }
  
  async alertaError() {
    const alert = await this.alertController.create({
      header: 'ERROR!',
      message: 'Lo sentimos esta funcion aun no esta disponible',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Lo sentimos esta funcion aun no esta disponible');
          }
        }
      ]
    });

    await alert.present();
  }
}
