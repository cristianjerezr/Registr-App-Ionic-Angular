import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRouteSnapshot, NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  modeloUsuario: string;
  modeloContrasena: string;
  contrasena: string;
  usuario: string;
  
  constructor(public navController: NavController, private alertController: AlertController,
    public router: Router, public api: ApiService, private dbService: DbService, private toastController: ToastController) {
      

         

    }
  
  ngOnInit() {
  }


  async verFormularioRegistro() {
    const alert = await this.alertController.create({
      header: 'Formulario de Registro!',
      inputs: [
        {
          name: 'correo',
          type: 'text',
          placeholder: 'Usuario'
        },
        {
          name: 'contrasena',
          type: 'password',
          placeholder: 'Contraseña'
        }        
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Guardar',
          handler: (data) => {
            this.registrarUsuario(data.correo, data.contrasena);

            this.api.crearUsuario(data.correo, data.contrasena).subscribe(data => {
            console.log('Confirm Ok');
            });
            this.presentAlertaConfirmRegistro();
          }
        }
      ]
    });

    await alert.present();
  }

  registrarUsuario(correo, contrasena) {
    this.dbService.validarRegistro(correo).then((data) => {
      if (!data) {
        console.log('se guarda');
        this.dbService.registrarUsuario(correo, contrasena);
      } else {
        this.presentToastRegistro();
      }    
    })
  }


  async presentToastRegistro() {
    const toast = await this.toastController.create({
      message: 'Usuario ya existe',
      duration: 2000
    });
    toast.present();
  }

  validacionDbAndApi(){
    this.validacionLogin();
    this.validarLoginApi();
  }

  validacionLogin() {
    this.dbService.validarLogin(this.modeloUsuario, this.modeloContrasena).then((data) => {
      if (!data) {
        

        this.presentAlertaConfirmValidacionDb();
        

        
        var navigationExtras: NavigationExtras = {
          state: {
            nombre: this.modeloUsuario,
            pass: this.modeloContrasena
          }
        }
  
        
        this.router.navigate(['inicio'], navigationExtras);
      } else {
        this.presentToastValidar();
        this.presentAlertError();
      }    
    })

  }


  validarLoginApi() {
    this.api.validarLogin(this.modeloUsuario, this.modeloContrasena).subscribe(data => {
      console.log('CJR',data);
      // LOGIN OK -> REDIRECCIONAR AL INICIO
      // LOGIN NOK -> ENVIAR MENSAJE DE CREDENCIALES INVÁLIDAS

      if(data.result === 'LOGIN OK') {

        var navigationExtras: NavigationExtras = {
          state: {
            nombre: this.modeloUsuario,
            pass: this.modeloContrasena
          }
        }
        
        
        this.presentAlertaConfirmValidacionApi();
        this.router.navigate(['inicio'], navigationExtras);
      } else {

        console.log('CJR',data);
        this.presentToastValidar();
        
      }
    })
  }


  ver404(){
    this.router.navigate(['**']);
  }



  async presentToastValidar() {
    const toast = await this.toastController.create({
      message: 'Usuario o contraseña incorrecta',
      duration: 2000
    });
    toast.present();
  }

  async presentAlertaConfirmValidacionDb() {
    const alert = await this.alertController.create({
      header: 'OK!',
      message: 'Datos validados Correctamente en DB',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Datos validados Correctamente en DB');
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertaConfirmValidacionApi() {
    const alert = await this.alertController.create({
      header: 'OK!',
      message: 'Datos validados Correctamente en API',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Datos validados Correctamente en API');
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertaConfirmRegistro() {
    const alert = await this.alertController.create({
      header: 'OK!',
      message: 'Usuario Almacenado Correctamente',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Usuario Almacenado Correctamente');
          }
        }
      ]
    });

    await alert.present();
  }  

  async presentAlertError() {
    const alert = await this.alertController.create({
      header: 'ERROR!',
      message: 'Usuario o Contraseña Invalidos',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Usuario o Contraseña Invalidos');
          }
        }
      ]
    });

    await alert.present();
  }


  
}
