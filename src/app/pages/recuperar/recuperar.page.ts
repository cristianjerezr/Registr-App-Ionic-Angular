import { Component, OnInit, Output } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-recuperar',
  templateUrl: './recuperar.page.html',
  styleUrls: ['./recuperar.page.scss'],
})
export class RecuperarPage implements OnInit {

  modeloUsuario: string;
  modeloContrasena: string;
  nuevaContrasena: string;

  constructor(public navController: NavController, public alertController: AlertController,
    public router: Router, private api: ApiService, private dbService: DbService) { }

  ngOnInit() {
  }

  

  validarCambio(){
    this.cambiarContraseñaApi();
    this.cambiarContraseñaDb();
    var navigationExtras: NavigationExtras = {
      state: {
        nombre: this.modeloUsuario,
        pass: this.modeloContrasena
      }
    }

    
    this.router.navigate(['inicio'], navigationExtras);
  }

  cancelarCambio(){
    var navigationExtras: NavigationExtras = {
      state: {
        nombre: this.modeloUsuario,
        pass: this.modeloContrasena
      }
    }

    
    this.router.navigate(['inicio'], navigationExtras);
  }



  cambiarContraseñaApi() {
    this.api.validarLogin(this.modeloUsuario, this.modeloContrasena).subscribe(data => {
      console.log('CJR',data);
      // LOGIN OK -> REDIRECCIONAR AL INICIO
      // LOGIN NOK -> ENVIAR MENSAJE DE CREDENCIALES INVÁLIDAS

      if(data.result === 'LOGIN OK') {

        this.api.modificarContrasena(this.modeloUsuario, this.nuevaContrasena).subscribe(data => {});;
        this.alertaConfirmacionApi();
      } else {

        console.log('CJR',data);
        this.alertaError();
        
      }
    })
  }

  cambiarContraseñaDb() {
    this.dbService.validarLogin(this.modeloUsuario, this.modeloContrasena).then((data) => {
      if (!data) {

        
        this.dbService.cambiarContrasena(this.nuevaContrasena, this.modeloContrasena);
        this.alertaConfirmacionDb();
        
        
        
        
      } else {
        this.alertaError();
      }    
    })



  }


  async alertaConfirmacionDb() {
    const alert = await this.alertController.create({
      header: 'OK!',
      message: 'Datos actualizados Correctamente en DB',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Datos actualizados Correctamente en DB');
          }
        }
      ]
    });

    await alert.present();
  }

  async alertaConfirmacionApi() {
    const alert = await this.alertController.create({
      header: 'OK!',
      message: 'Datos actualizados Correctamente en API',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Datos actualizados Correctamente en API');
          }
        }
      ]
    });

    await alert.present();
  }

  async alertaError() {
    const alert = await this.alertController.create({
      header: 'ERROR!',
      message: 'El usuario o la contraseña son incorrectos',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('El usuario o la contraseña son incorrectos');
          }
        }
      ]
    });

    await alert.present();
  }
}
